(define-package "helm" "20220226.2016" "Helm is an Emacs incremental and narrowing framework"
  '((helm-core "3.8.4")
    (popup "0.5.3"))
  :commit "f030e584b7f2c8679297ce960cedbdf0f7e7e86e" :authors
  '(("Thierry Volpiatto" . "thierry.volpiatto@gmail.com"))
  :maintainer
  '("Thierry Volpiatto" . "thierry.volpiatto@gmail.com")
  :url "https://emacs-helm.github.io/helm/")
;; Local Variables:
;; no-byte-compile: t
;; End:
